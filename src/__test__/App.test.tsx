import React from 'react';
import { shallow } from 'enzyme';

import App from "../App";

describe("App component", () => {
    afterAll(() => {
        jest.clearAllMocks();
    })

    it("should render", () => {
        const mock = shallow(<App />);
        expect(mock).toMatchSnapshot();
    });

    it("should call getCryptos", async () => {
        const mock = shallow(<App />);
        const instance  = mock.instance() as App;
        const getCryptosMock = jest.spyOn(instance, "getCryptos");
        
        await instance.componentDidMount();

        expect(getCryptosMock).toBeCalled();
    });
    
    it("should call the api handler", () => {
        const mock = shallow(<App />);
        const instance  = mock.instance() as App;
        const getLatestMock = jest.spyOn(instance.cryptocurrencyListings, "getLatest");
        const setStateMock = jest.spyOn(instance, "setState");
        
        instance.getCryptos();
        
        expect(getLatestMock).toBeCalled();
        expect(setStateMock).toBeCalled();
    });
})