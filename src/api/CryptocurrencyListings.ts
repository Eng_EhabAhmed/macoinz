import Config from 'react-native-config';

export interface CoinData {
    id: number;
    name: string;
    symbol: string;
    slug: string;
    num_market_pairs: number;
    date_added: string;
    tags: string[];
    max_supply: number;
    circulating_supply: number;
    total_supply: number;
    platform: string | null;
    cmc_rank: number;
    last_updated: string;
    quote: {
        USD: {
            price: number;
            volume_24h: number;
            percent_change_1h: number;
            percent_change_24h: number;
            percent_change_7d: number;
            market_cap: number;
            last_updated: string;
        }
    }
}

export interface Error {
    errorCode: number;
    errorMessage: string;
}

export default class CryptocurrencyListings {
    private baseURL = Config.API_URL;
    private servicesRoutes = {
        latest: "/latest",
    }

    private API_KEY = Config.API_KEY;

    getLatest = async (): Promise<CoinData[]> => {
        return new Promise<CoinData[]>(async (resolve, reject) => {
            fetch(this.baseURL + this.servicesRoutes.latest, {
                headers: {
                    "X-CMC_PRO_API_KEY": this.API_KEY
                }
            })
            .then(async (apiResponse) => {
                const responseJson = await apiResponse.json();
            
                if (responseJson.status.error_code === 0) {
                    resolve(responseJson.data);
                } else {
                    reject({
                        errorCode: responseJson.status.error_code,
                        errorMessage: responseJson.status.error_message,
                    } as Error);
                }
            })
            .catch((e) => {
                reject({
                    errorCode: -1,
                    errorMessage: "No Internet Connection",
                } as Error);
            });
        });
    }
}