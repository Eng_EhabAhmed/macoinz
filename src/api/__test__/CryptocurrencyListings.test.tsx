import Config from 'react-native-config';

import CryptocurrencyListings from "../CryptocurrencyListings";

describe("API Handler", () => {
    it("should call the api with the right params", () => {
        const cryptocurrencyListings = new CryptocurrencyListings();
        const fetchMock = jest.spyOn(global, "fetch").mockImplementation((input, init) => {
            return Promise.resolve(new Response({
                status: {
                    error_code: 0,
                },
                data: []
            }));
        });

        cryptocurrencyListings.getLatest().then(() => {
            expect(fetchMock).toBeCalledWith(
                Config.API_URL + "/latest",
                {
                    headers: {
                        "X-CMC_PRO_API_KEY": Config.API_KEY
                    }
                }
            )
        })
    });
})