import React, { Component } from 'react';
import {
    FlatList,
    Alert
} from 'react-native';

import CryptocurrencyListings, { CoinData } from './api/CryptocurrencyListings';
import CryptocurrencyListItem from './components/CryptocurrencyListItem';
import LoadingPlaceHolder from './components/LoadingPlaceHolder';
import APIError from './components/APIError';

interface State {
    loading: boolean;
    data: CoinData[];
    isModalVisable: boolean;
    currentCoin: CoinData;
    errorMSG?: string;
}

class App extends Component<{}, State> {
    cryptocurrencyListings = new CryptocurrencyListings();
    state = {
        loading: true,
        data: [],
        isModalVisable: false,
        currentCoin: {} as CoinData,
        errorMSG: undefined,
    }

    async componentDidMount() {
        this.getCryptos();
    }

    getCryptos = () => {
        this.setState({
            loading: true,
        });

        this.cryptocurrencyListings.getLatest()
        .then((data) => {
            this.setState({
                data,
                loading: false,
                errorMSG: undefined,
            });
        })
        .catch((error) => {
            this.setState({
                loading: false,
                errorMSG: error.errorMessage,
            });
        });
    }

    /* istanbul ignore next */
    renderListItem = ({item}: {item: CoinData}) => (
        <CryptocurrencyListItem
            data={item}
        />
    )

    render() {
        const {
            data,
            loading,
            errorMSG,
        } = this.state;

        return (
            loading ? (
                <LoadingPlaceHolder />
            ) : (
                <>
                    <FlatList
                        data={data}
                        renderItem={this.renderListItem}
                        keyExtractor={
                            /* istanbul ignore next */
                            (item) => item.id.toString()
                        }
                        ListEmptyComponent={<APIError onRertyPress={this.getCryptos} msg={errorMSG}/>}
                    />
                </>
            )
        );
    }
};

export default App;
