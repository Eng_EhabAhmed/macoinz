import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Dimensions } from "react-native";

import { primaryColor } from "../constants/Pallete";

interface Props {
    onRertyPress: () => void;
    msg?: string;
}

export default class APIError extends Component<Props> {
    render() {
        const { msg, onRertyPress } = this.props;

        return(
            <View style={styles.container}>
                <View style={styles.msgContainer}>
                    <Text style={styles.msg}>
                        {msg || "Something Went Wrong Please Try Again Later."}
                    </Text>
                </View>
                <TouchableOpacity style={styles.button} onPress={onRertyPress}>
                    <Text style={styles.buttonTitle}>Retry</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: Dimensions.get("window").height - 24
    },
    msgContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        paddingVertical: 10,
        backgroundColor: primaryColor,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonTitle: {
        color: "#FFF",
        fontWeight: 'bold',
    },
    msg: {
        color: "grey"
    }
})