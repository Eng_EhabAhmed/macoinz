import React, { Component } from "react";
import { View, StyleSheet, Image, Animated } from "react-native";

const AnimatedImage = Animated.createAnimatedComponent(Image);

interface State {
    isIconVisiable: boolean;
}

export default class LoadingPlaceHolder extends Component<{}, State> {
    iconFade = new Animated.Value(1);
    state = {
        isIconVisiable: true,
    }

    componentDidMount() {
        this.animate();
    }

    animate = () => {
        const { isIconVisiable } = this.state;

        const toValue = isIconVisiable ? 0 : 1;
        Animated.timing(this.iconFade, {toValue, duration: 600}).start(this.animate);
        this.setState({isIconVisiable: !isIconVisiable})
    }

    render() {
        return (
            <View style={styles.container}>
                <AnimatedImage
                    source={require("../assets/logo.png")}
                    style={[{
                        opacity: this.iconFade
                    }, styles.icon]}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: {
        height: 100,
        width: 100,
    }
})