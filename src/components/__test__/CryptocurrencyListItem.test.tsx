import React from 'react';
import { shallow } from 'enzyme';

import CryptocurrencyListItem from "../CryptocurrencyListItem";
import { CoinData } from '../../api/CryptocurrencyListings';

describe("API Error Display component", () => {
    const btc = {
        id: 1,
        name: "Bitcoin",
        symbol: "BTC",
        slug: "bitcoin",
        num_market_pairs: 7815,
        date_added: "2013-04-28T00:00:00.000Z",
        tags: [
            "mineable"
        ],
        max_supply: 21000000,
        circulating_supply: 17877650,
        total_supply: 17877650,
        platform: null,
        cmc_rank: 1,
        last_updated: "2019-08-15T09:07:32.000Z",
        quote: {
            USD: {
                price: 9884.39679101,
                volume_24h: 21632713314.4264,
                percent_change_1h: -0.426017,
                percent_change_24h: -6.13967,
                percent_change_7d: -16.6632,
                market_cap: 176709786290.79993,
                last_updated: "2019-08-15T09:07:32.000Z"
            }
        }
    } as CoinData;
    
    it("should render with 'down-arrow'", () => {
        const mock = shallow(<CryptocurrencyListItem data={btc} />);
        expect(mock).toMatchSnapshot();
    })
    
    it("should render with 'up-arrow'", () => {
        btc.quote.USD.percent_change_7d = 16;
        const mock = shallow(<CryptocurrencyListItem data={btc} />);
        expect(mock).toMatchSnapshot();
    })
})