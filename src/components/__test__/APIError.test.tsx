import React from 'react';
import { shallow } from 'enzyme';

import APIError from "../APIError";

describe("API Error Display component", () => {
    it("should render", () => {
        const mock = shallow(<APIError onRertyPress={() => null} />);
        expect(mock).toMatchSnapshot();
    })

    it("should render with custom message", () => {
        const mock = shallow(<APIError onRertyPress={() => null} msg="custom message" />);
        expect(mock).toMatchSnapshot();
    })
})