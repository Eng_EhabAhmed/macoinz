import React from 'react';
import { shallow } from 'enzyme';

import LoadingPlaceHolder from "../LoadingPlaceHolder";
import { Animated } from 'react-native';

describe("Loading Placeholder component", () => {
    it("should render", () => {
        const mock = shallow(<LoadingPlaceHolder />);
        expect(mock).toMatchSnapshot();
    })

    it("should call animate", () => {
        const mock = shallow(<LoadingPlaceHolder />);
        const instance = mock.instance() as LoadingPlaceHolder;

        const animateMock = jest.spyOn(instance, 'animate');

        instance.componentDidMount();
        expect(animateMock).toBeCalled();
    })
    
    it("should start the animation", () => {
        const mock = shallow(<LoadingPlaceHolder />);
        const instance = mock.instance() as LoadingPlaceHolder;

        const animateMock = jest.spyOn(Animated, 'timing');

        instance.animate();
        expect(animateMock).toBeCalled();
    })

})