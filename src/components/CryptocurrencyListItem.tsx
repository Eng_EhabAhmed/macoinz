import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome5';

import { CoinData } from "../api/CryptocurrencyListings";

interface Props {
    data: CoinData;
}

export default class CryptocurrencyListItem extends Component<Props> {
    render() {
        const { data } = this.props;
        
        return (
            <View style={styles.container}>
                <View style={styles.flexRow}>
                    <Text style={styles.name}>
                        {data.name}
                    </Text>
                    <Text>
                        {data.symbol}
                    </Text>
                </View>
                <View style={styles.flexRow}>
                    <Text>
                        <Text style={styles.arrowIcon}>
                        {
                            data.quote.USD.percent_change_7d > 0 ?
                                <Icon name="arrow-up" size={12} color="#090" /> :
                                <Icon name="arrow-down" size={12} color="#900" />
                        }
                        </Text>
                        {Math.round(data.quote.USD.percent_change_7d * 100) / 100}%
                    </Text>
                    <Text>
                        {Math.round(data.quote.USD.price * 100) / 100}$
                    </Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 15,
        backgroundColor: "#FFF",
        borderBottomColor: "#000",
        borderBottomWidth: 0.5,
    },
    flexRow: {
        flexDirection: 'row',
        justifyContent: "space-between",
        marginBottom: 5,
    },
    name: {
        fontWeight: "bold",
        fontSize: 15,
    },
    arrowIcon: {
        marginRight: 50,
    }
})