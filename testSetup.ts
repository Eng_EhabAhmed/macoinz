import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-enzyme';
import * as fetch from 'jest-fetch-mock';
import 'react-native/jest/setup';

(global as any).fetch = fetch;

configure({ adapter: new Adapter() });
